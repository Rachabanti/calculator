<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/Custom/cal_block/templates/my-template.html.twig */
class __TwigTemplate_561e59dbc6f24641217efb21c8d8ea535a30a77834e501cfa1c5dd20fda1e7bf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"hero is-primary has-text-centered\">
  <div class=\"hero-body\">
    <div class=\"container\">
      <h1 class=\"title is-1\">Ovulation Predictor Calculator</h1>
    </div>
  </div>
  </section>
  
  <section id=\"app\" class=\"section content has-text-centered\">
    
<transition name=\"fade\" mode=\"out-in\">
    <div class=\"content columns level is-centered
\" v-if=\"!calcReturned\" key=\"calendar\">

      <div class=\"column is-one-third level has-text-centered \">
        <p class=\"subtitle\">Please select the first day of your last menstrual period:</p>
        <vuejs-datepicker :inline=\"true\" v-model=\"date\">";
        // line 17
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["date"] ?? null), 17, $this->source), "html", null, true);
        echo "</vuejs-datepicker>
      </div>

      <div class=\"column is-one-third has-text-centered level\">
        <p>Usual number of days in your cycle:</p>
        <div class=\"select is-primary\">
          <select name=\"days\" v-model=\"cycleSelected\">
            <option v-for=\"n in 45\" v-if=\"n >= 20\">";
        // line 24
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["n"] ?? null), 24, $this->source), "html", null, true);
        echo "</option>
          </select>
        </div>

        <div class=\"level-item\">
          <button class=\"button is-large is-primary is-rounded \" id=\"calculate-btn\" @click=\"startCalc(); calculateFertileBegin(); calculateFertileEnds(); calculateDueDate();\">Calculate</button>
        </div>
      </div>
    </div>

    <div class=\"content has-text-centered\" v-else key=\"result\">

      <p class=\"subtitle\">Here are the results based on the information you provided:</p>
      <p>Your next most fertile period is <strong>";
        // line 37
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["fertileFrom"] ?? null), 37, $this->source), "html", null, true);
        echo "</strong> to <strong>";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["fertileUntil"] ?? null), 37, $this->source), "html", null, true);
        echo "</strong>.</p>
      <p>If you conceive within this timeframe, your estimated due date will be <strong>";
        // line 38
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["dueDate"] ?? null), 38, $this->source), "html", null, true);
        echo "</strong>.</p>
      <div class=\"level-item\">
        <button class=\"button is-large is-primary is-rounded\" id=\"resetCalc\" @click=\"resetCalc\">Calculate Again</button>
      </div>

    </div>
</transition>
    
    <div class=\"notification is-full\">*Average length will vary by woman. **A woman's best days to conceive can start at least one day prior and last at least one day after fertile date.
    </div>

  </section>";
    }

    public function getTemplateName()
    {
        return "modules/Custom/cal_block/templates/my-template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 38,  83 => 37,  67 => 24,  57 => 17,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/Custom/cal_block/templates/my-template.html.twig", "C:\\xampp\\htdocs\\drupal_dev\\web\\modules\\Custom\\cal_block\\templates\\my-template.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 17);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
